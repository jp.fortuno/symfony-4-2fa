# symfony-4-2fa

Based on the scheb/two-factor-bundle

- To try it out, use the register link after running the project
```html
/register
```
- Then login with the user:
```html
/login
```
- install the Google Authenticator App from Play Store or Apple Store
- scan the QR code generated after the login 
- enter the code to test
